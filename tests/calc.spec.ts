import { expect } from "chai";
import { sum ,multiply} from "../src/calc";
describe("Basic Mocha String Test", function () {
    context("#sum", () => {
        it("should exist", () => {
            expect(sum).to.be.a("function");
        });
        it("should sum two numbers", () => {
            const actual = sum(2, 3);
            expect(actual).to.equal(5);
        });

        it("should sum more than numbers", () => {
            const actual = sum(2, 3,4,5);
            expect(actual).to.equal(14);
        });


    });


    context("#multiply", () => {
        it("should exist", () => {
            expect(multiply).to.be.a("function");
        });
        it("should multiply two numbers", () => {
            const actual = multiply(2, 3);
            expect(actual).to.deep.equal([6]);
        });

        it("should multiply more than two numbers", () => {
            const actual = multiply(2,1,2,3);
            expect(actual).to.deep.equal([2,4,6]);
        });


    });





});




