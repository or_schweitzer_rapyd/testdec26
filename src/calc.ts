export function sum(...numbers:number[]){

    return numbers.reduce((sum,item) => {
        return sum +item;
        });
}

export function multiply (multiplier:number,...numbers:number[]){

    return numbers.map(item => item * multiplier);
}